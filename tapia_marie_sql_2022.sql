-----------------------------------   Projet fin de Semestre SQL -----------------------------------
-----------------------------------------------------------------------------------------------------

--------- Requête 1 :
--Afficher les patients nés à partir du premier janvier 1990
select * from patient where date_naissance >= '1990-01-01';


---------- Requête 2 :
--Afficher les patients nés à partir du premier janvier 1990 et de sexe féminin.
select * from patient where date_naissance >= '1990-01-01' and sexe = 'F';

----------- Requête 3 :
--Afficher les patients nés à partir du premier janvier 1990, de sexe féminin et résidant dans les villes
--avec id 1, 3 et 5.
select * from patient where date_naissance >= '1990-01-01' and sexe = 'F' and id_ville in (1,3,5);


---------Requête 4 :
--Afficher les patients résidant dans les villes Lille et Roubaix. Pour cette requête, ne pas utiliser
--directement les identifiants des villes mais plutôt leurs noms (Lille et Roubaix)

select * from patient p left join ville v on p.id_ville =v.id_ville where ville 
	in  ('Lille','Roubaix');


---------Requête 5 :
--Compter le nombre de patients résidant dans les villes Lille et Roubaix (pour chacune des villes).
--Pour cette requête, ne pas utiliser directement les identifiants des villes mais plutôt leurs noms (Lille
--et Roubaix)
select ville, count(id_patient) as nb_patient from patient p left join ville v on p.id_ville =v.id_ville 
	where ville in  ('Lille','Roubaix') 
	group by ville;

---------Requête 6 :
--Compter le nombre de séjours débutant chaque jour, en les triant par date de début.
select date_debut_sejour ,count(id_sejour) as nb_sejour from sejour 
	group by date_debut_sejour 
	order by date_debut_sejour ; 

---------Requête 7 :
--Compter le nombre de séjours débutant chaque jour, en les triant par date de début et en ne
--conservant que les jours ayant plus de 2 séjours.
--Réponse attendue :

 select date_debut_sejour ,count(id_sejour) as nb_sejour  from sejour 
	group by date_debut_sejour having count(id_sejour) > '2'
	order by date_debut_sejour ; 



--------Requête 8 :
--Afficher les RUMs du patient n°1

select * from rum r left join sejour s on s.id_sejour = r.id_sejour where id_patient = '1' ; 



---------Requête 9 :
--Insérer 3 RUMs pour le séjour n°11. Vous pouvez choisir les dates, modes d'admission et mode de
--sortie. Le tout doit être cohérent.

select * from sejour ;
select * from rum;

INSERT INTO rum  (id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
VALUES(6, 11, '2020-01-10', '2020-01-13', 8, 8),
	  (7, 11, '2020-01-13', '2020-01-16', 8, 8),
	  (8, 11, '2020-01-16', '2020-01-20', 8, 8);

--------Requête 10 :
--Insérer 1 acte pour chacun des RUMs du séjours n°11.
	
select * from rum_acte ;
select * from acte;

INSERT INTO rum_acte  (id_acte,id_rum, date_acte)
VALUES(4, 6, '2020-01-10'),
	  (4, 7, '2020-01-13'),
	  (5, 8, '2020-01-16');



--------Requête 11 :
--Insérer 1 diagnostic pour chacun des RUMs du séjours n°11.
	 
select * from rum_diagnostic;

INSERT INTO rum_diagnostic  (id_diagnostic,id_rum, date_diagnostic)
VALUES(5, 6, '2020-01-13'),
	  (3, 7, '2020-01-16'),
	  (3, 8, '2020-01-20');	 
	 


-------Requête 12 :
--Créer une table MEDICAMENT permettant de stocker une liste de médicaments.

	 drop table if exists medicament; 
	 drop table if exists rum_med; 
	
CREATE TABLE medicament (
	id_med			int NOT NULL,
	nom_medicament			VARCHAR (50) not null,
	CONSTRAINT pk_med PRIMARY KEY (id_med)
);


INSERT INTO medicament  (id_med, nom_medicament)
VALUES(1, 'paracetamol'),
	  (2, 'morphine');	

	 select * from medicament; 

--------Requête 13 :
--Créer une table RUM_MEDICAMENT permettant de stocker des administrations de médicaments
--lors de RUM.

	 CREATE TABLE rum_med (
	id_med 			int NOT NULL,
	id_rum				int not null,
	id_admninistation			int not null,
	CONSTRAINT pk_rum_med PRIMARY KEY (id_med, id_rum,id_admninistation),
	constraint fk_rum_med_rum foreign key (id_rum) references rum (id_rum),
	constraint fk_rum_med_med foreign key (id_med) references medicament (id_med)
);

select * from rum_med; 


-------Requête 14 :
--Insérer 5 administrations de médicaments dans la table RUM_MEDICAMENT, pour le séjour 11.

INSERT INTO rum_med (id_med, id_rum,id_admninistation)
VALUES(1,6,1),
	  (1,6,2),
	  (1,6,3),
	  (2,7,4),
	  (1,8,5);	

	 select * from rum_med; 

	--- on imagine un table administation avec un identifiant nommé id_administration dans laquelle 
	--  on retrouve l'heure , la date d'administration et la dose. 

